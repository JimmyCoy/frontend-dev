import './Form.css';
import {useState} from 'react';

const Form = () => {
  const [name, setName] = useState({text: localStorage.getItem('name') ?? '', errorText: ''});
  const [rating, setRating] = useState({text: localStorage.getItem('rating') ?? '', errorText: ''});
  const [review, setReview] = useState(localStorage.getItem('review') ?? '');

  const handleSubmit = (event) => {
    event.preventDefault();

    if (name.text.length === 0) {
        setName(prevName => ({...prevName, errorText: 'Вы забыли указать имя и фамилию'}))
        return;
    }
    if (name.text.length < 2) {
        setName(prevName => ({...prevName, errorText: 'Имя не может быть короче 2-х символов'}))
        return;
    }
    setName(prevName => ({...prevName, errorText: ''}));

    if (+rating.text < 1 || +rating.text > 5 || rating.text.length === 0) {
      setRating(prevRating => ({...prevRating, errorText: 'Оценка должна быть от 1 до 5'}))
      return;
    }
    setRating(prevRating => ({...prevRating, errorText: ''}));

    localStorage.removeItem('name');
    setName(prevName => ({...prevName, text: ''}));
    localStorage.removeItem('rating');
    setRating(prevRating => ({...prevRating, text: ''}));
    localStorage.removeItem('review');
    setReview('');
  }

  const changeNameText = (event) => {
    setName(prevName => ({...prevName, text: event.target.value}));
    localStorage.setItem('name', event.target.value);
  }
  const changeRatingText = (event) => {
    setRating(prevRating => ({...prevRating, text: event.target.value}));
    localStorage.setItem('rating', event.target.value);
  }
  const changeReviewText = (event) => {
    setReview(event.target.value);
    localStorage.setItem('review', event.target.value);
  }
  const nameFocus = () => {
    setName(prevName => ({...prevName, errorText: ''}))
  }
  const ratingFocus = () => {
    setRating(prevRating => ({...prevRating, errorText: ''}))
  }

  const nameClasses = `error ${name.errorText ? 'error-visible' : ''}`
  const ratingClasses = `error ${rating.errorText ? 'error-visible' : ''}`
    return (
    <form onSubmit={handleSubmit} className="review-form">
      <div className="input-container">
        <input onFocus={nameFocus} onChange={changeNameText} className="input input-name" type="text" name="name" placeholder="Имя и фамилия" value={name.text}/>
        <div className={nameClasses}>{name.errorText}</div>
      </div>
      <div className="input-container input-container_rating">
        <input onFocus={ratingFocus} onChange={changeRatingText} className="input input-rating" type="number" name="rating" placeholder="Оценка" value={rating.text}/>
        <div className={ratingClasses}>{rating.errorText}</div>
      </div>
      <textarea onChange={changeReviewText} className="input input_textarea" name="review-text" placeholder="Текст отзыва" value={review}></textarea>
      <button className="form-submit" type="submit">Отправить отзыв</button>
  </form>
    );
}

export default Form;