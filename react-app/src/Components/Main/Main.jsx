import Form from '../Form/Form';
import ColorOptions from '../ColorOptions/ColorOptions';
import MemoryConfigs from '../MemoryConfigs/MemoryConfigs';
import './Main.css';

const Main = ({ cartCount, favoriteCount, changeCart, changeFavorite }) => {
  const cartBtnStyles = { background: !!cartCount ? '#888' : '#F36223' }
  const favoriteBtnStyles = { 
    stroke: !!favoriteCount ? '#F36223' : '#888', 
    fill: !!favoriteCount ? '#F36223' : 'transparent'
  }
  return (
    <div className="container">
      <nav className="breadcrumbs">
        <a href="#">Электроника</a>
        <span>{'>'}</span>
        <a href="#">Смартфоны и гаджеты</a>
        <span>{'>'}</span>
        <a href="#">Мобильные телефоны</a>
        <span>{'>'}</span>
        <a href="#">Apple</a>
      </nav>

      <main className="main">
        <h2 className="product-name">Смартфон Apple iPhone 13, синий</h2>
        <section className="product-images">
          <img src="../../Images/image-1.webp" alt="Вид смартфона спереди и сзади" />
          <img src="../../Images/image-2.webp" alt="Экран смартфона" />
          <img src="../../Images/image-3.webp" alt="Вид смартфона сбоку" />
          <img src="../../Images/image-4.webp" alt="Блок камер смартфона" />
          <img src="../../Images/image-5.webp" alt="Задняя панель смартфона" />
        </section>

        <div className="main__specifications-box">
          <div className="all-specifications">
            <div className="color-option">
              <ColorOptions />
            </div>

            <div className="memory-config">
              <MemoryConfigs />
            </div>

            <section className="product-properties">
              <h3 className="product-properties__title">Характеристики товара</h3>
              <ul className="product-properties__list">
                <li>Экран: <b>6.1</b></li>
                <li>Встроенная память: <b>128 ГБ</b></li>
                <li>Операционная система: <b>iOS 15</b></li>
                <li>Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b></li>
                <li>Процессор: <a href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank"><b>Apple A15
                  Bionic</b></a></li>
                <li>Вес: <b>173 г</b></li>
              </ul>
            </section>

            <section className="product-description">
              <h3 className="product-description__title">Описание</h3>
              <div className="product-description__paragraphs">
                <p>
                  Наша самая совершенная система двух камер.
                  <br /> Особый взгляд на прочность дисплея.
                  <br />Чип, с которым всё супербыстро.
                  <br />Аккумулятор держится заметно дольше.
                  <br /><i>iPhone 13 - сильный мира всего.</i>
                </p>
                <p>Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. Благодаря
                  этому внутри корпуса поместилась наша лучшая система двух камер с увеличенной матрицей широкоугольной
                  камеры. Кроме того, мы освободили место для системы оптической стабилизации изображения сдвигом матрицы.
                  И повысили скорость работы матрицы на сверхширокоугольной камере.</p>
                <p>Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков. Новая широкоугольная
                  камера улавливает на 47% больше света для более качественных фотографий и видео. Новая оптическая
                  стабилизация со сдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
                <p>Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения фокуса и изменения
                  резкости. Просто начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки,
                  создавая красивый эффект размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести
                  фокус на другого человека или объект, который появился в кадре. Теперь ваши видео будут смотреться как
                  настоящее кино.</p>
              </div>
            </section>

            <section className="comparison">
              <h3 className="comparison__title">Сравнение моделей</h3>
              <table className="comparison__table">
                <thead>
                  <tr className="comparison__table-head">
                    <th>Модель</th>
                    <th>Вес</th>
                    <th>Высота</th>
                    <th>Ширина</th>
                    <th>Толщина</th>
                    <th>Чип</th>
                    <th>Объём памяти</th>
                    <th>Аккумулятор</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="comparison__table-row">
                    <td>iPhone 11</td>
                    <td>194 грамма</td>
                    <td>150.9 мм</td>
                    <td>75.7 мм</td>
                    <td>8.3 мм</td>
                    <td>A13 Bionic chip</td>
                    <td>до 128 Гб</td>
                    <td>До 17 часов</td>
                  </tr>
                  <tr className="comparison__table-row">
                    <td>iPhone 12</td>
                    <td>164 грамма</td>
                    <td>146.7 мм</td>
                    <td>71.5 мм</td>
                    <td>7.4 мм</td>
                    <td>A14 Bionic chip</td>
                    <td>до 256 Гб</td>
                    <td>До 19 часов</td>
                  </tr>
                  <tr className="comparison__table-row">
                    <td>iPhone 13</td>
                    <td>174 грамма</td>
                    <td>146.7 мм</td>
                    <td>71.5 мм</td>
                    <td>7.65 мм</td>
                    <td>A15 Bionic chip</td>
                    <td>до 512 Гб</td>
                    <td>До 19 часов</td>
                  </tr>
                </tbody>
              </table>
            </section>

            <section className="review-section">
              <div className="review-section__header">
                <h3 className="review-section__title">Отзывы</h3>
                <span className="review-section__count">425</span>
              </div>

              <div className="review-section__list">
                <div className="review">
                  <img className="review__photo" src="../../Images/review-1.jpeg" alt='Фото' />

                  <div className="review__content">
                    <h4 className="review__name">Марк Г.</h4>
                    <img className="review__rating" src="../../Images/rating-5.png" alt='Рейтинг' />

                    <div className="review__paragraphs">
                      <p><b>Опыт использования:</b> менее месяца</p>
                      <p><b>Достоинства:</b> <br />это мой первый айфон после огромного количества телефонов на андроиде.
                        всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на
                        высоте</p>
                      <p><b>Недостатки:</b> <br />к самому устройству мало имеет отношение, но перенос данных с андроида -
                        адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает
                        качество фотографий исходное</p>
                    </div>
                  </div>
                </div>

                <div className="separator"></div>

                <div className="review">
                  <img className="review__photo" src="../../Images/review-2.jpeg" alt='Фото' />

                  <div className="review__content">
                    <h4 className="review__name">Валерий Коваленко</h4>
                    <img className="review__rating" src="../../Images/rating-4.png" alt='Рейтинг' />

                    <div className="review__paragraphs">
                      <p><b>Опыт использования:</b> менее месяца</p>
                      <p><b>Достоинства:</b> <br />OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго</p>
                      <p><b>Недостатки:</b> <br />Плохая ремонтопригодность</p>
                    </div>
                  </div>
                </div>
              </div>
            </section>

            <div className="add-review">
              <h3 className="add-review__title">Добавить свой отзыв</h3>
              <div className="add-review__box">
                <Form />
              </div>
            </div>
          </div>

          <aside className="sidebar">
            <div className="purchase-info">
              <button onClick={changeFavorite} className="add-to-favorite purchase-info__add-to-favorite">
                <svg style={favoriteBtnStyles} className="favorite-icon" width="30" height="30" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
                  <path d="M23.6152 16.493L25.0002 17.8227L26.3853 16.493L30.6517 12.3973C30.6518 12.3973 30.6518 12.3973 30.6518 12.3972C33.8674 9.3103 39.0916 9.31029 42.3072 12.3972C45.4767 15.44 45.4767 20.3173 42.3073 23.3602C42.3073 23.3602 42.3072 23.3602 42.3072 23.3603L25.0002 39.9748L7.69349 23.3602C7.69348 23.3602 7.69348 23.3602 7.69347 23.3602C4.52387 20.3173 4.5239 15.44 7.69347 12.3972C10.909 9.3103 16.1332 9.31029 19.3488 12.3972C19.3488 12.3972 19.3488 12.3972 19.3488 12.3972L23.6152 16.493Z" strokeWidth="4" />
                </svg>
              </button>
              <div className="purchase-info__price">
                <div className="purchase-info__old-price">
                  <s className="gray-text">75 990₽</s>
                  <span className="discount">-8%</span>
                </div>
                <div className="purchase-info__new-price">67 990₽</div>
              </div>
              <div className="purchase-info__delivery">
                <span>Самовывоз в четверг, 1 сентября — <b>бесплатно</b></span>
                <span>Курьером в четверг, 1 сентября — <b>бесплатно</b></span>
              </div>
              <button style={cartBtnStyles} onClick={changeCart} className="cart-button">{!!cartCount ? 'Товар уже в корзине' : 'Добавить в корзину'}</button>
            </div>
            <div className="ads">
              <div className="ads__title gray-text">Реклама</div>
              <div className="ads__frames">
                <iframe className="ads__iframe" title='frame1' src='../../ads.html'></iframe>
                <iframe className="ads__iframe" title='frame2' src='../../ads.html'></iframe>
              </div>
            </div>
          </aside>
        </div>
      </main>
    </div>
  );
}

export default Main;  