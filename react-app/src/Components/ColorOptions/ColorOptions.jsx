import './ColorOptions.css';
import {useState} from 'react';

const ColorOptions = () => {
  const [selected, setSelected] = useState('Синий')

  const changeSelected = (alt) => {
    setSelected(alt);
  }
  return (
  <>
  <h3 className="color-option__name">Цвет товара: {selected}</h3>
  <div className="color-option__list">
    {colors.map((color) => {
      const selectedClass = selected === color.alt ? "color-option__btn_selected" : "";
      return (
      <button onClick={changeSelected.bind(null, color.alt)} key={color.alt} className={`color-option__btn ${selectedClass}`}>
        <img className="color-image" src={color.img} alt={color.alt}/>
      </button>
      );
    })}
  </div>
  </>
  );
}

const colors = [
  {
    img: '../../Images/color-1.webp',
    alt: 'Красный'
  },
  {
    img: '../../Images/color-2.webp',
    alt: 'Зеленый'
  },
  {
    img: '../../Images/color-3.webp',
    alt: 'Розовый'
  },
  {
    img: '../../Images/color-4.webp',
    alt: 'Синий'
  },
  {
    img: '../../Images/color-5.webp',
    alt: 'Белый'
  },
  {
    img: '../../Images/color-6.webp',
    alt: 'Черный'
  }
]

export default ColorOptions;