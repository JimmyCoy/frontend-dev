import './MemoryConfigs.css';
import {useState} from 'react';

const MemoryConfigs = () => {
  const [selected, setSelected] = useState('128 ГБ')

  const changeSelected = (value) => {
    setSelected(value);
  }
  return (
  <>
  <h3 className="memory-config__name">Конфигурация памяти: {selected}</h3>
  <div className="memory-config__buttons">
    {memoryOptions.map((memory) => {
      const selectedClass = selected === memory.value ? "memory-button_selected" : "";
      return (
        <button onClick={changeSelected.bind(null, memory.value)} key={memory.id} className={`memory-button ${selectedClass}`}>
          {memory.value}
        </button>
      );
    })}
  </div>
  </>
  );
}

const memoryOptions = [
  {
    id: '0',
    value: '128 ГБ'
  },
  {
    id: '1',
    value: '256 ГБ'
  },
  {
    id: '2',
    value: '512 ГБ'
  }
]

export default MemoryConfigs;