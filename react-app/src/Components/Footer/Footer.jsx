import './Footer.css';

const Footer = () => {
    return (
    <footer className="footer">
      <div className="footer-content container">
        <div className="footer-content__copyright">
          <strong>© ООО «<span className="carrot-text">Мой</span>Маркет», 2018-2022</strong>
          <div>
            Для уточнения информации звоните по номеру
            <a href="tel:79000000000">+7 900 000 0000</a>,
          </div>
          <div>
            а предложения по сотрудничеству отправляйте на почту
            <a href="mailto:partner@mymarket.com">partner@mymarket.com</a>
          </div>
        </div>
        <div><a href="#up">Наверх</a></div>
      </div>
    </footer>
    );
}
    
export default Footer;