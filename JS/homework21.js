"use strict";

// Упражнение 1

let user1 = {};
let user2 = {name: 'Arina',};

/**
 * Проверяет наличие свойств у объекта.
 * @param {object} obj Любой объект.
 * @returns {boolean} Возвращает true, если у объекта нет свойств, иначеfalse.
 */
function isEmpty(obj) {
    for (let key in obj) {
        if (key in obj) return false;
    }
    return true;
}
console.log(isEmpty(user1));
console.log(isEmpty(user2));

// Упражнение 2 в файле data.js

// Упражнение 3

let salaries = {
    John:100000,
    Ann:160000,
    Pete:130000,
};
/**
 * Производит повышение зарплаты на определенный процент 
 * и возвращает объект с новыми зарплатами.
 * @param {number} percent Процент, на который повышается зарплата.
 * @param {number} raise Число, на которое повышается зарплата.
 * @returns {object} Объект с увеличенными зарплатами.
 */
function raiseSalary(percent) {
    let newSalaries = {};
    for (let key in salaries) {
        let raise = salaries[key] * percent / 100;
        newSalaries[key] = salaries[key] + raise;
        Math.floor(newSalaries[key]);
    }
    return newSalaries;
}
/**
 * Производит подсчет общего бюджета.
 * @param {object} obj Объект, в котором нужно посчитать бюджет.
 * @returns {number} суммарное значение всех зарплат.
 */
function calcSum(obj) {
    let sum = 0;
    for (let key in obj) {
        sum = sum + obj[key];
    }
return sum;
}
let newSalaries = raiseSalary(5);
let sum = calcSum(newSalaries);
console.log(newSalaries, sum);