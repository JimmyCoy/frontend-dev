import {useState} from 'react';
import Header from '../Header/Header';
import Main from '../Main/Main';
import Footer from '../Footer/Footer';
import './PageProduct.css';

const PageProduct = () => {
  const [cartCount, setCartCount] = useState(+localStorage.getItem('cart') ?? 0);
  const [favoriteCount, setFavoriteCount] = useState(+localStorage.getItem('favorite') ?? 0);

  const changeCart = () => {
    setCartCount(prevCount => prevCount ? 0 : 1);
  }
  const changeFavorite = () => {
    setFavoriteCount(prevCount => prevCount ? 0 : 1);
  }
  localStorage.setItem('cart', cartCount);
  localStorage.setItem('favorite', favoriteCount);

  return (
    <>
      <Header cartCount={cartCount} favoriteCount={favoriteCount} />
      <Main cartCount={cartCount} favoriteCount={favoriteCount} changeCart={changeCart} changeFavorite={changeFavorite}/>
      <Footer />
    </>
  );
}

export default PageProduct;
