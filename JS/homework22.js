"use strict";

// Упражнение 1

let arr1 = [1, 2, 3];
let arr2 = ['a', {}, 4, 6];

function getSum(arr) {
    let sum = arr.filter(Number).reduce(function(prev, current) {
        return prev + current;
    }, 0);
    return sum;
}
console.log(getSum(arr1)); // 6
console.log(getSum(arr2)); // 10

// Упражнение 2 в файле data.js

// Упражнение 3

// В корзине один товар
let cart = [4884];

function addToCart(productId) {
    let isInCart = cart.includes(productId);
    if (isInCart) return;
    cart.push(productId);
}
// Добавили товар
addToCart(3456);
addToCart(3456); // Два одинаковых товара не добавляются
console.log(cart); // [4884, 3456]

function removeFromCart(productId) {
    cart = cart.filter(function(id) {
        return id !== productId;
    });
}
// Удалили товар
removeFromCart(4884);
console.log(cart); // [3456]