"use strict";

let form = document.querySelector('.review-form');
let inputName = form.querySelector('.input-name');
let inputRating = form.querySelector('.input-rating');
let inputText = form.querySelector('.input_textarea');
let errorNameElem = form.querySelector('.error-name');
let errorRatingElem = form.querySelector('.error-rating');

inputName.value = localStorage.getItem('name') ?? '';
inputRating.value = localStorage.getItem('rating') ?? '';
inputText.value = localStorage.getItem('review-text') ?? '';

function handleSubmit(event) {
    event.preventDefault();
    let name = inputName.value;
    let rating = inputRating.value;

    function addError(elem, error) {
        const errorElem = elem.nextElementSibling;
        errorElem.innerText = error;
        errorElem.classList.add('error-visible');
    }
    if (name.length === 0) {
        const errorName = 'Вы забыли указать имя и фамилию';
        addError(inputName, errorName);
        return;
    }
    if (name.length < 2) {
        const errorName = 'Имя не может быть короче 2-х символов';
        addError(inputName, errorName);
        return;
    }
    if (rating < 1 || rating > 5 || rating.length === 0) {
        const errorRating = 'Оценка должна быть от 1 до 5';
        addError(inputRating, errorRating);
        return;
    }
    localStorage.removeItem('name');
    localStorage.removeItem('rating');
    localStorage.removeItem('review-text');
    inputName.value = '';
    inputRating.value = '';
    inputText.value = '';
}
function focusHandle(event) {
    const target = event.target;
    if (target.closest('input')) {
        const targetError = target.nextElementSibling;
        targetError.classList.remove('error-visible');
    }
}

function inputHandle(event) {
    const target = event.target;
    const targetName = target.getAttribute('name');
    const targetValue = target.value;
    localStorage.setItem(targetName, targetValue);
}

form.addEventListener('submit', handleSubmit);
form.addEventListener('focus', focusHandle, true);
form.addEventListener('input', inputHandle);

let cartButton = document.querySelector('.cart-button');
let cartCount = document.querySelector('.cart-count');

let productInCart = JSON.parse(localStorage.getItem('productInCart')) ?? false;
if (productInCart) {
    cartCount.textContent = '1';
    cartButton.style.background = '#888888';
    cartButton.textContent = 'Товар уже в корзине';
}
function clickHandler() {
    if (!productInCart) {
        cartCount.textContent = '1';
        cartButton.style.background = '#888888';
        cartButton.textContent = 'Товар уже в корзине';
        localStorage.setItem('productInCart', true);
        productInCart = true;
    } else {
        cartCount.textContent = '0';
        cartButton.style.background = '#F36223';
        cartButton.textContent = 'Добавить в корзину';
        localStorage.setItem('productInCart', false);
        productInCart = false;
    }
}
cartButton.addEventListener('click', clickHandler);

let favoriteButton = document.querySelector('.favorite-icon');
let favoriteCount = document.querySelector('.favorite-count');

let productInFavorite = JSON.parse(localStorage.getItem('productInFavorite')) ?? false;
if (productInFavorite) {
    favoriteCount.textContent = '1';
    favoriteButton.style.stroke = '#F36223';
    favoriteButton.style.fill = '#F36223';
}
function heartClickHandler() {
    if (!productInFavorite) {
        favoriteCount.textContent = '1';
        favoriteButton.style.stroke = '#F36223';
        favoriteButton.style.fill = '#F36223';
        localStorage.setItem('productInFavorite', true);
        productInFavorite = true;
    } else {
        favoriteCount.textContent = '0';
        favoriteButton.style.stroke = '#888888';
        favoriteButton.style.fill = 'transparent';
        localStorage.setItem('productInFavorite', false);
        productInFavorite = false;
    }
}
favoriteButton.addEventListener('click', heartClickHandler);