import './Header.css';

const Header = ({cartCount, favoriteCount}) => {
  
    return (
    <header className="header">
      <div className="container header__content">
        <a className='main-link' href='./main-page.html'>
          <div>
            <img className="header__logo" src="../../Icons/favicon.svg"/>
            <h1 className="header__title">
              <span className="carrot-text">Мой</span>Маркет
            </h1>
          </div>
        </a>
        <div className="header__actions">
          <button className="header__button-icon">
            <svg width="50" height="50" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path 
                fillRule="evenodd" clipRule="evenodd" 
                d="M3.78502 6.57269C6.17872 4.27474 10.0466 4.27474 12.4403 6.57269L15.0001 9.03017L17.56 6.57269C19.9537 4.27474 23.8216 4.27474 26.2154 6.57269C28.609 8.87064 28.609 12.5838 26.2154 14.8818L15.0001 25.6483L3.78502 14.8818C1.39132 12.5838 1.39132 8.87064 3.78502 6.57269ZM10.6725 8.26974C9.25515 6.90905 6.97018 6.90905 5.55278 8.26974C4.1354 9.63044 4.1354 11.824 5.55278 13.1848L15.0001 22.2542L24.4476 13.1848C25.865 11.824 25.865 9.63044 24.4476 8.26974C23.0302 6.90905 20.7452 6.90905 19.3279 8.26974L15.0001 12.4243L10.6725 8.26974Z"/>
            </svg>
            {!!favoriteCount && <span className="item-count">{favoriteCount}</span>}
          </button>
          <button className="header__button-icon">
            <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path 
                fillRule="evenodd" clipRule="evenodd" 
                d="M4.79102 5.91504H12.0882H13.7961L14.1311 7.5228L15.0838 12.0962H43.4231H45.8823L45.4781 14.425L43.3319 26.7872L43.0417 28.4584H41.2769H18.4927L19.3763 32.7H41.2769V36.7H17.6684H15.9605L15.6255 35.0922L11.333 14.4884L13.3759 14.0962L11.333 14.4884L10.3803 9.91504H4.79102V5.91504ZM15.9172 16.0962L17.6593 24.4584H39.5121L40.9638 16.0962H15.9172ZM23.2829 41.6778C23.2829 43.5186 21.7325 45.007 19.815 45.007C17.8974 45.007 16.347 43.5186 16.347 41.6778C16.347 39.837 17.8974 38.3876 19.815 38.3876C21.7325 38.3876 23.2829 39.837 23.2829 41.6778ZM36.9863 45.007C38.9038 45.007 40.4542 43.5186 40.4542 41.6778C40.4542 39.837 38.9038 38.3876 36.9863 38.3876C35.0686 38.3876 33.5181 39.837 33.5181 41.6778C33.5181 43.5186 35.0686 45.007 36.9863 45.007Z"/>
            </svg>
            {!!cartCount && <span className="item-count">{cartCount}</span>}
          </button>
        </div>
      </div>  
    </header>
    );
}

export default Header;